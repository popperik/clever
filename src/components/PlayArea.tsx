import React, { useContext, useEffect } from 'react';
import DieView from './Die';
import PinkArea from './PinkArea';
import { Context } from '../Context';

import './PlayArea.css';
import Die from '../Die';
import UsedDice from './UsedDice';
import BlueArea from './BlueArea';
import SilverPlatter from './SilverPlatter';
import GreenArea from './GreenArea';
import RollButton from './RollButton';
import TurnCounter from './TurnCounter';
import EndTurnButton from './EndTurnButton';
import SilverArea from './SilverArea';
import YellowArea from './YellowArea';

const dice = [
    { name: 'Blue',   colour: '#0c4da2' },
    { name: 'Silver', colour: '#939598' },
    { name: 'Yellow', colour: '#e8ef26' },
    { name: 'Pink',   colour: '#ea4f97' },
    { name: 'Green',  colour: '#8dc63f' },
    { name: 'White',  colour: '#ffffff' }
] as Die[];

export default function () {
    const context = useContext(Context);

    useEffect(() => {
        context.initialiseDice(context, dice);
    }, []);

    const dieViews = context.availableDice.map(d =>
        <DieView key={`playArea-dieView-${d.name}`} die={d} />);

    return <React.StrictMode>
        <div style={{display: 'flex'}}>
            <div style={{width: '50%'}}>
                <div className="topArea">
                    <UsedDice />
                    <TurnCounter />
                </div>
                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                    <SilverArea />
                    <YellowArea />
                </div>
                <BlueArea />
                <GreenArea />
                <PinkArea />
            </div>
            <div>
                <div style={{display: 'flex'}}>
                    {dieViews}
                </div>
                <br />
                <SilverPlatter />
                <RollButton />
                <EndTurnButton />
            </div>
        </div>
    </React.StrictMode>;
}
