import React from "react";
import { useContext } from "react"
import { Context, GameState } from "../Context"

export default function() {
    const context = useContext(Context);
    if (context.state !== GameState.WaitingForRoll) {
        return <></>
    }

    const roll = () => {
        context.rollDice(context);
    }

    return <button onClick={roll}>Roll</button>
}