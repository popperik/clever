import React from "react";
import { useContext } from "react";
import { Context } from "../Context";

import "./TurnCounter.css";

export default function() {
    const context = useContext(Context);

    const turnCells = [];
    for (let i = 0; i < 6; ++i) {
        let cross = <></>;
        if (i < context.currentRound) {
            cross = <div className="cross">X</div>;
        }

        turnCells.push(
            <div className="turnCell" key={`turnCell-${i}`}>
                {cross}
                <div className="turnNumber">{i+1}</div>
            </div>);
    }

    return <div style={{display: 'flex'}}>
        {turnCells}
    </div>
}