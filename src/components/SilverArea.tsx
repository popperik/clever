import { useContext, useState } from "react";
import { Context, GameState } from "../Context";
import Cell from "./Cell";
import Die from "../Die";
import React from "react";
import Star from "./Star";

import "./SilverArea.css";

export default function() {
    const context = useContext(Context);
    const [cells, setCells] = useState([[],[],[],[]] as boolean[][]); // row, column
    const [dieForSecondSelection, setDieForSecondSelection] = useState(null as Die | null);
    const colours = ['yellow', 'blue', 'green', 'pink'];
    let points = 0;

    function isSelectable(row: number, col: number): boolean {
        if (dieForSecondSelection?.value === col + 1) {
            return !cells[row][col]; // It's selectable if it's empty;
        }

        if (!context.selectedDie) {
            return false;
        }

        if (context.selectedDie.name !== 'White' && context.selectedDie.name !== 'Silver') {
            return false;
        }

        if (col !== context.selectedDie.value - 1) {
            return false;
        }

        return !cells[row][col]; // It's selectable if it's empty
    }

    function onSelected(row: number, col: number) {
        cells[row][col] = true;

        // if this is the first die chosen, we need to handle all the smaller ones
        if (context.selectedDie) {
            const ad = context.availableDice
                .filter(d => d.value < context.selectedDie!.value);

            ad.forEach(d => {
                const c = colours.indexOf(d.name.toLowerCase());
                if (c === -1) {
                    // It's the white/silver one
                    setDieForSecondSelection(d);
                    // Selecting an additional field is kinda like a bonus?
                    context.setContext({...context, state: GameState.BonusSelection });
                } else {
                    cells[c][d.value - 1] = true;
                }
            });
        }

        // if this is the second die (i.e. both a white and silver were chosen), we do nothing
        if (dieForSecondSelection) {
            setDieForSecondSelection(null);
            context.setContext({...context, state: GameState.WaitingForRoll});
        }

        setCells([...cells]);
    }

    const rows = [];
    for (let row = 0; row < 4; ++row) {
        const rowCells = [];
        for (let col = 0; col < 6; ++col) {
            const value = cells[row][col] ? 'X' : '';
            rowCells.push(
                <div key={`silver-${row}-${col}`} className={`silverCellHolder ${colours[row]}`}>
                    <Cell backgroundText={col+1} selectable={isSelectable(row, col)} onSelected={() => onSelected(row, col)} value={value} />
                </div>
            );
        }

        rows.push(<div key={`silver-${row}`} className="row">{rowCells}</div>);
    }

    const scores = [0, 2, 4, 7, 11, 16, 22];
    let scoreHolders = [];
    for (let i = 1; i <= 6; ++i) {
        scoreHolders.push(<span key={`silver-points-${i}`} className="silverPointHolder">
            {i} <Star fill="#939598" value={scores[i]}/>
        </span>);
    }

    rows.push(<div key="silver-points" className="silverPointsHolder">{scoreHolders}</div>);

    return <div className="silverArea">
        {rows} ∑ {points}
    </div>
}
