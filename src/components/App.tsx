import React, { useState } from 'react';
import { Context, ContextType, GameState } from '../Context';
import PlayArea from './PlayArea';
import Die from '../Die';

function randomRoll() {
    return Math.floor(Math.random() * 6 + 1);
}

export default function () {
    let [context, updateContext] = useState({
        dice: [],
        availableDice: [],
        currentRound: 0,
        selectedDie: undefined,
        silverPlatterDice: [],
        state: GameState.WaitingForRoll,
        usedDice: [],

        endTurn(context: ContextType) {
            context.currentRound++;
            context.availableDice = context.dice;
            context.usedDice = [];
            context.silverPlatterDice = [];
            context.selectedDie = undefined;
            context.state = GameState.WaitingForRoll;
            updateContext({...context});
        },

        initialiseDice(context: ContextType, dice: Die[]) {
            context.dice = dice;
            context.availableDice = dice;
            context.state = GameState.WaitingForRoll;
            context.currentRound = 1;
            updateContext({...context});
        },

        rollDice(context: ContextType) {
            context.availableDice.forEach(d => d.value = randomRoll());
            context.state = GameState.DieSelection;
            updateContext({...context});
        },

        setContext(context: ContextType) {
            updateContext({...context});
        },

        useSelectedDie(context: ContextType) {
            if (!context.selectedDie) {
                throw new Error('useSelectedDie called without a die selected');
            }

            const newSilverPlatterDice = context.availableDice.filter(d => d.value < context.selectedDie!.value);
            context.silverPlatterDice.push(...newSilverPlatterDice);

            context.availableDice = context.availableDice.filter(d =>
                d.name !== context.selectedDie!.name &&
                d.value >= context.selectedDie!.value
            );

            context.usedDice.push(context.selectedDie);
            context.selectedDie = undefined;

            // Only allow the next roll if we were just waiting for a die selection. If something has set it to
            // something else (e.g. selecting a bonus), then that will need to finish and that code will need to update
            // the state
            if (context.state === GameState.DieSelection && context.usedDice.length < 3 && context.availableDice) {
                context.state = GameState.WaitingForRoll;
            }

            updateContext({...context});
        }
    } as ContextType);

    return <React.StrictMode>
        <Context.Provider value={context}>
            <PlayArea />
        </Context.Provider>
    </React.StrictMode>;
}
