import React from 'react';
import { useContext } from 'react';
import { Context, GameState } from '../Context';
import Die from '../Die';
import './Die.css';

interface Props {
    die: Die
    selected?: boolean
    selectable?: boolean // If set, will override default logic
}

export default function(props: Props) {
    const context = useContext(Context);
    let className = 'die';
    if (context.selectedDie?.name === props.die.name) {
        className += ' selected';
    }

    const onClick = () => {
        if (props.selectable !== false && context.state == GameState.DieSelection) { // Can't select a die if we're waiting for a roll
            context.setContext({ ...context, selectedDie: props.die});
        }
    }

    return <div className={className} style={{backgroundColor: props.die.colour ?? 'white '}} onClick={onClick}>
        {props.die.value}
    </div>
}
