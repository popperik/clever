import React from "react";
import { useContext, useState } from "react";
import { Context } from "../Context";
import Cell from "./Cell";
import "./PinkArea.css";

export default function() {
    const context = useContext(Context);
    const cellCount = 12;
    const [cells, setCells] = useState([] as number[]);
    const sum = cells.reduce((sum, current) => sum + current, 0);
    const cellViews = [];
    const cellBackgrounds = [0, 0, 2, 3, 4 , 5, 6, 2, 3, 4, 5, 6];

    const onSelected = () => {
        setCells([...cells, context.selectedDie?.value!]);
    }

    for (let i = 0; i < cellCount; ++i) {
        const selectable = (context.selectedDie?.name === 'Pink' || context.selectedDie?.name === 'White') && i === cells.length;
        const value = i < cells.length ? cells[i] : '';
        let backgroundTextAttribute = {};
        if (cellBackgrounds[i]) {
            backgroundTextAttribute = {backgroundText: `≥${cellBackgrounds[i]}`};
        }
        cellViews.push(<Cell key={`pink-${i}`} value={value} selectable={selectable} onSelected={onSelected}
            {...backgroundTextAttribute} />);
    }

    return <div className="pinkArea">
        {cellViews} ∑ {sum}
    </div>
}
