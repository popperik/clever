import { useContext, useState } from "react";
import { Context, GameState } from "../Context";
import Cell from "./Cell";
import Die from "./Die";
import React from "react";
import Star from "./Star";

import "./YellowArea.css";

export default function() {
    const context = useContext(Context);

    const rows = [];
    const scores = [0, 3, 10, 21, 36, 55, 75, 96, 118, 141, 165];
    let scoreHolders = [];

    const [cells, setCells] = useState([] as string[]);

    const boxProperties = [
        {value: 3, x: 1, y: 0}, {value: 6, x: 3, y: 0},
        {value: 1, x: 0, y: 1}, {value: 2, x: 2, y: 1},
        {value: 4, x: 1, y: 2}, {value: 3, x: 3, y: 2},
        {value: 2, x: 0, y: 3}, {value: 5, x: 2, y: 3},
        {value: 5, x: 1, y: 4}, {value: 4, x: 3, y: 4},
    ];

    const isSelectable = (i: number) => {
        if (context.selectedDie?.name != 'Yellow' && context.selectedDie?.name != 'White') {
            return false;
        }

        if (context.selectedDie.value !== boxProperties[i].value) {
            return false;
        }

        if (cells[i] === 'X') {
            return false;
        }
        return true;
    }

    const onSelected = (i: number) => {
        cells[i] = cells[i] ? 'X' : 'O';
        setCells([...cells]);
    }

    // First the top row
    for (let i = 1; i <= 5; ++i) {
        scoreHolders.push(<span key={`yellow-points-${i}`} className="yellowPointHolder yellowPointHolderFirstRow">
            {i} <Star fill="#ffc40a" textColor="black" value={scores[i]}/>
        </span>);
    }

    rows.push(<div key="yellow-points-top" className="yellowPointsHolder">{scoreHolders}</div>);

    // Then the bottom row
    scoreHolders = [];

    // First the top row
    for (let i = 6; i <= 10; ++i) {
        scoreHolders.push(<span key={`yellow-points-${i}`} className="yellowPointHolder">
            {i} <Star fill="#ffc40a" textColor="black" value={scores[i]}/>
        </span>);
    }

    rows.push(<div key="yellow-points-bottom" className="yellowPointsHolder">{scoreHolders}</div>);

    const boxes = [];
    for (let i = 0; i < boxProperties.length; ++i) {
        const box = boxProperties[i];
        const x = 15 + box.x * 100;
        const y = 100 + box.y * 35;
        boxes.push(<Cell backgroundText={box.value} value={cells[i]} style={{position: 'absolute', left: x, top: y}}
         selectable={isSelectable(i)} onSelected={() => onSelected(i)} />);
    }
    rows.push(<div>{boxes}</div>);

    const points = scores[cells.filter(c => c).length]; // Count all non-null cells
    return <div className="yellowArea">
        {rows} ∑ {points}
    </div>
}
