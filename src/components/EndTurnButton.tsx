import React from "react";
import { useContext } from "react"
import { Context } from "../Context"

export default function() {
    const context = useContext(Context);
    return <button onClick={() => context.endTurn(context)}>End turn</button>
}