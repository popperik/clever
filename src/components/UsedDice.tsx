import React from "react";
import { useContext } from "react";
import { Context } from "../Context";
import DieSlot from "./DieSlot";

import "./UsedDice.css";

export default function() {
    const context = useContext(Context);

    const dieSlots = [];
    for (let i = 0; i < 3; ++i) {
        dieSlots.push(<DieSlot key={`dieSlot-${i}`} die={context.usedDice[i]} />);
    }

    return <div className="usedDice">
        {dieSlots}
    </div>
}