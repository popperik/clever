import React from "react";
import { useContext } from "react";
import { Context } from "../Context";
import Die from "./Die";
import "./SilverPlatter.css";

export default function() {
    const context = useContext(Context);

    const styles: React.CSSProperties[] = [ // We can have a maximum of 5 dice on the silver platter
        {left: '75px', top: '50px'},
        {left: '175px', top: '50px'},
        {left: '50px', top: '150px'},
        {left: '125px', top: '150px'},
        {left: '200px', top: '150px'},
    ];
    const dice = [];
    for (let i = 0; i < context.silverPlatterDice.length; ++i) {
        const die = context.silverPlatterDice[i];
        dice.push(<span key={`silvePlatterDie-${die.name}`} style={{position: 'absolute', ...styles[i]}}>
            <Die die={die} selectable={false} />
        </span>);
    }

    return <div className="silverPlatter">
        {dice}
    </div>
}
