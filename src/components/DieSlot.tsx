import React from 'react';
import Die from '../Die';
import './DieSlot.css';

interface Props {
    die: Die | undefined
}

export default function(props: Props) {
    const colour = props.die?.colour ?? 'whitesmoke';
    return <div className="outerBorder">
        <div className="dieSlot" style={{backgroundColor: colour}}>{props.die?.value}</div>
    </div>
}