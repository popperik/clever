import React from 'react';
import { useContext, useEffect, useState } from 'react';
import { Context } from '../Context';
import './Cell.css';

interface Props {
    backgroundText?: number | string;
    value?: number | string;
    selectable: boolean;
    style?: React.CSSProperties
    onSelected: () => void;
}

export default function(props: Props) {
    const context = useContext(Context);
    let [dieHandlingDone, setDieHandlingDone] = useState(false);

    let className = 'cell';
    if (props.selectable) {
        className += ' selectable';
    }

    let cellValue = <></>;
    if (props.value) {
        cellValue = <div className='cellValue'>{props.value}</div>
    }

    const onClick = () => {
        if (props.selectable) {
            props.onSelected();
            setDieHandlingDone(true);
        }
    };

    useEffect(() => {
        if (dieHandlingDone && context.selectedDie) {
            context.useSelectedDie(context);
        }
        setDieHandlingDone(false);
    }, [dieHandlingDone]);

    return <div className="cellHolder" onClick={onClick} style={props.style}>
        {cellValue}
        <div className={className}>{props.backgroundText}</div>
    </div>
}
