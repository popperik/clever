import React from "react";
import "./Star.css";

interface Props {
    fill: string;
    textColor?: string;
    value?: number;
}

export default function(props: Props) {
    const style = {fill: props.fill, stroke: 'white', strokeWidth: '10px'} as React.CSSProperties;
    return <div className="star">
        <div className="starText" style={{color: props.textColor ?? 'white'}}>
            {props.value}
        </div>
        <svg width="38" height="38">
            <g transform="scale(0.15)">
                <path style={style} d="m 188,209 -11.72143,34.18816 -32.00179,-16.79607 -26.26684,24.82489 -20.53066,-29.74415 -34.79484,9.77453 -4.35619,-35.8782 -35.35175,-7.51505 12.81621,-33.792999 -27.81,-23.083019 27.05258,-23.966225 -13.89732,-33.362951 35.09153,-8.6490759 3.19907,-35.9998341 35.09144,8.649473 19.56259,-30.389588 27.05232,23.966531 31.44456,-17.817453 12.81583,33.793145 36.12296,-1.163555 -4.35661,35.878158 32.52603,15.756899 -20.53099,29.743917 21.47776,29.067638 -32.00198,16.795699 5.50922,35.71934 z"/>
            </g>
        </svg>
    </div>
}
