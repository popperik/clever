import { useContext, useState } from "react";
import { Context } from "../Context";
import Cell from "./Cell";
import React from "react";
import Star from "./Star";

import "./BlueArea.css";

export default function() {
    const context = useContext(Context);
    const cellCount = 12;
    const [cells, setCells] = useState([] as number[]);
    const pointValues = [0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78];
    const points = pointValues[cells.length];
    const cellItems = [];

    const onSelected = () => {
        const whiteBlueSum = context.dice.find(d => d.name === 'Blue')!.value + context.dice.find(d => d.name === 'White')!.value;
        setCells([...cells, whiteBlueSum]);
    }

    function isSelectable(cellIndex: number) {
        if (!context.dice) {
            return false; // Not initialised yet
        }

        if (context.selectedDie?.name !== 'Blue' && context.selectedDie?.name !== 'White') {
            return false;
        }

        if (cellIndex !== cells.length) {
            return false; // Only the last empty cell can be selected
        }

        if (cells.length === 0) {
            return true; // The first cell doesn't have the <= requirement
        }

        const whiteBlueSum = context.dice.find(d => d.name === 'Blue')!.value + context.dice.find(d => d.name === 'White')!.value;
        return whiteBlueSum <= cells[cells.length - 1];
    }

    for (let i = 0; i < cellCount; ++i) {
        const value = i < cells.length ? cells[i] : '';
        let geq = <span key={`blue-geq-${i}`} className="geq">≥</span>;
        if (i === cellCount - 1) {
            geq = <></>; // No >= after the last item
        }

        cellItems.push(
            <div key={`blue-${i}`} className="blueCellHolder">
                <Star fill="#0c4da2" value={pointValues[i +1 ]} />
                <Cell value={value} selectable={isSelectable(i)} onSelected={onSelected} />
                {geq}
            </div>
        );
    }

    return <div className="blueArea">
        {cellItems} ∑ {points}
    </div>
}
