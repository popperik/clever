import React from "react";
import { useContext, useState } from "react";
import { Context } from "../Context";
import Cell from "./Cell";
import "./GreenArea.css";
import Star from "./Star";

export default function() {
    const context = useContext(Context);
    const cellCount = 12;
    const [cells, setCells] = useState([] as number[]);
    let points = 0;
    const cellViews = [];
    const multipliers = [2, 2, 2, 1, 3, 3, 3, 2, 3, 1, 4, 1];

    const onSelected = () => {
        setCells([...cells, context.selectedDie!.value * multipliers[cells.length]]);
    }

    for (let i = 0; i < cellCount; ++i) {
        if (i % 2 === 1 && cells[i]) { // Only calculate points for every second item in a pair
            points += cells[i-1] - cells[i];
        }

        let star = <></>;
        let minus = <></>;
        if (i % 2 === 0) { // We need a divider between every pair
            let value = undefined;
            if (cells[i+1]) { // If the next value is present, we can fill in the star
                value = cells[i] - cells[i+1];
            }

            star = <Star fill="#8cc63f" value={value} />;
            minus = <span className="minus">-</span>;
        }

        const value = i < cells.length ? cells[i] : '';
        const selectable = i === cells.length && (context.selectedDie?.name === 'Green' || context.selectedDie?.name === 'White');
        cellViews.push(
            <div key={`green-${i}`} className="greenCellHolder">
                {star}
                {minus}
                <Cell key={`green-${i}`} backgroundText={`x${multipliers[i]}`} value={value} selectable={selectable} onSelected={onSelected} />
            </div>
        );
    }

    return <div className="greenArea">
        {cellViews} ∑ {points}
    </div>
}
