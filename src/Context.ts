import React from "react";
import Die from "./Die";

export enum GameState {
    BonusSelection,
    DieSelection,
    WaitingForRoll
}

export interface ContextType {
    dice: Die[];
    availableDice: Die[];
    currentRound: number;
    selectedDie: Die | undefined;
    silverPlatterDice: Die[];
    state: GameState;
    usedDice: Die[];

    endTurn: (context: ContextType) => void;
    initialiseDice: (context: ContextType, dice: Die[]) => void;
    rollDice: (context: ContextType) => void;
    setContext: (context: ContextType) => void;
    useSelectedDie: (context: ContextType) => void;
}

export const Context = React.createContext<ContextType>({
    dice: [],
    availableDice: [],
    currentRound: 0,
    selectedDie: undefined,
    silverPlatterDice: [],
    state: GameState.WaitingForRoll,
    usedDice: [],

    endTurn: () => {},
    initialiseDice: () => {},
    rollDice: () => {},
    setContext: () => {},
    useSelectedDie: () => {}
} as ContextType);

