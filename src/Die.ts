export default interface Die {
    colour: string
    name: string
    value: number
}